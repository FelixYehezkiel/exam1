package org.example;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class MyCalendar {

    public void showMonth() {


        Calendar cal = new GregorianCalendar(2020, 8, 1);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        int dayWeek = cal.get(Calendar.DAY_OF_WEEK);
        int dayMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

        System.out.println();
        System.out.println("========================");
        System.out.print("BULAN ");
        System.out.println(new SimpleDateFormat("MMMM").format(cal.getTime()));
        System.out.println(" M  S  S  R  K  J  S");

        String space = "";
        for (int i = 0; i < dayWeek - 1; i++) {
            space += "-- ";
        }
        System.out.print(space);

        for (int i = 0, dayOfMonth = 1; dayOfMonth <= dayMonth; i++) {
            for (int j = ((i == 0) ? dayWeek - 1 : 0); j < 7 && (dayOfMonth <= dayMonth); j++) {
                System.out.printf("%2d ", dayOfMonth);
                dayOfMonth++;

            }
            System.out.println();
        }
    }

    public void showYear(){

        int year = 2020;
        int getFirstDay = 3;

        for ( int month = 1; month <= 12; month++ ){
            int days = 0;
            String monthName = " ";
            switch (month) {
                case 1: monthName = "Januari";
                    days = 31;
                    break;
                case 2: monthName = "Februari";
                    if(((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)){
                        days = 29;
                    } else {
                        days = 28;
                    }
                    break;
                case 3: monthName = "Maret";
                    days = 31;
                    break;
                case 4: monthName = "April";
                    days = 30;
                    break;
                case 5: monthName = "Mei";
                    days = 31;
                    break;
                case 6: monthName = "Juni";
                    days = 30;
                    break;
                case 7: monthName = "Juli";
                    days = 31;
                    break;
                case 8: monthName = "Agustus";
                    days = 31;
                    break;
                case 9: monthName = "September";
                    days = 30;
                    break;
                case 10: monthName = "Oktober";
                    days = 31;
                    break;
                case 11: monthName = "November";
                    days = 30;
                    break;
                case 12: monthName = "Desember";
                    days = 31;
                    break;
            }

            System.out.println("	BULAN	" + monthName );
            System.out.println("-----------------------------------");
            System.out.println("  M  S  S  R  K  J  S");

            int i = 0;
            int firstDay = 0;
            switch(month){
                case 1: firstDay=getFirstDay;
                    break;
                case 2: firstDay=getFirstDay+3;
                    break;
                case 3: firstDay=getFirstDay+3;
                    break;
                case 4: firstDay=getFirstDay+6;
                    break;
                case 5: firstDay = getFirstDay + 8;
                    break;
                case 6: firstDay = getFirstDay + 11;
                    break;
                case 7: firstDay = getFirstDay + 13;
                    break;
                case 8: firstDay = getFirstDay + 16;
                    break;
                case 9: firstDay = getFirstDay + 19;
                    break;
                case 10: firstDay = getFirstDay + 21;
                    break;
                case 11: firstDay = getFirstDay + 24;
                    break;
                case 12: firstDay = getFirstDay + 26;
                    break;
            }
            if(((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)){
                switch(month){
                    case 1: firstDay=getFirstDay;
                        break;
                    case 2: firstDay=getFirstDay+3;
                        break;
                    case 3: firstDay=getFirstDay+4;
                        break;
                    case 4: firstDay=getFirstDay+7;
                        break;
                    case 5: firstDay = getFirstDay + 9;
                        break;
                    case 6: firstDay = getFirstDay + 12;
                        break;
                    case 7: firstDay = getFirstDay + 14;
                        break;
                    case 8: firstDay = getFirstDay + 17;
                        break;
                    case 9: firstDay = getFirstDay + 20;
                        break;
                    case 10: firstDay = getFirstDay + 22;
                        break;
                    case 11: firstDay = getFirstDay + 25;
                        break;
                    case 12: firstDay = getFirstDay + 27;
                        break;
                }

            }
            int dayWeek = 0;
            if ( (firstDay % 7 ) >= 0 ){
                if ( (firstDay % 7 ) == 0 ){
                    dayWeek = 0;
                } else if ( (firstDay % 7 ) == 1 ){
                    dayWeek = 1;
                    System.out.print("   " );
                } else if ( (firstDay % 7 ) == 2 ){
                    dayWeek = 2;
                    System.out.print("\t  " );
                } else if ( (firstDay % 7 ) == 3 ){
                    dayWeek = 3;
                    System.out.print("\t\t " );
                } else if ( (firstDay % 7 )  == 4 ){
                    dayWeek = 4;
                    System.out.print("\t\t\t" );
                } else if ( (firstDay % 7 ) == 5 ){
                    dayWeek = 5;
                    System.out.print("\t\t\t   " );
                } else if ( (firstDay % 7 ) == 6 ){
                    dayWeek = 6;
                    System.out.print("\t\t\t\t  " );
                }
            }

            for ( i = 1; i <= days; i++ ) {
                if (i < 10)
                    System.out.print("  " + i );
                else
                    System.out.print(" " + i );

                if ((i + firstDay ) % 7 == 0 )
                    System.out.println();
            }
            System.out.println();
        }
    }
}

